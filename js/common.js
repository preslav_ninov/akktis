$(document).ready(function() {
    $('#example').DataTable( {
        "processing": false,
        "serverSide": false,
        "ajax": "./getData.php"
    } );
	
	var colorsHeader = $(".colors-data");
		
	colorsHeader.dblclick(function() {
	$( ".sorting_1" ).each(function( index ) {
	$( this ).removeClass("sorting_1");
	var colorData = $( this ).text();
	$( this ).addClass(colorData);
	});	
	});
	
} );