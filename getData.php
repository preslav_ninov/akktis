<?php
header('Content-type: application/json');

$servername = "localhost";
$username = "root";
$password = "password";
$dbname = "dev_schema";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT sp.id, sp.first_name, sp.last_name, sc.color_text FROM `simple_persons` sp
JOIN simple_colors AS sc
  ON sp.color_id = sc.id";
$result = $conn->query($sql);
$data = array();
$data["draw"] = 1;
$data["recordsTotal"] = $result->num_rows;
$data["recordsFiltered"] = $result->num_rows;
$data["data"] = array();

$count = 0;
if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
		$resultArray[$count] = array();
		array_push($resultArray[$count], $row["id"], $row["first_name"], $row["last_name"], $row["color_text"]);
		array_push($data["data"], $resultArray[$count]);
		$count++;
		}
			print_r(json_encode($data));

		} else {
		echo json_encode(array("success"=>"false"));
}
$conn->close();