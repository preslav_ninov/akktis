-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 
-- Версия на сървъра: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dev_schema`
--

-- --------------------------------------------------------

--
-- Структура на таблица `simple_colors`
--

CREATE TABLE `simple_colors` (
  `id` int(11) NOT NULL,
  `color_text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Схема на данните от таблица `simple_colors`
--

INSERT INTO `simple_colors` (`id`, `color_text`) VALUES
(1, 'red'),
(2, 'blue'),
(3, 'yellow'),
(4, 'green'),
(5, 'black'),
(6, 'white');

-- --------------------------------------------------------

--
-- Структура на таблица `simple_persons`
--

CREATE TABLE `simple_persons` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `color_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Схема на данните от таблица `simple_persons`
--

INSERT INTO `simple_persons` (`id`, `first_name`, `last_name`, `color_id`) VALUES
(1, 'Somename', 'Lastname', 2),
(2, 'Crazy', 'Max', 3),
(3, 'Barak', 'Obama', 5),
(4, 'Geri', 'Nikol', 6),
(5, 'Nencho', 'Balabanov', 2),
(6, 'Neli', 'Petkova', 2),
(7, 'Grisho', 'Grishkov', 4),
(8, 'Joro', 'Bicepsa', 1),
(9, 'Mitio', 'Krikov', 5),
(10, 'Mitio', 'Pishtova', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `simple_colors`
--
ALTER TABLE `simple_colors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `simple_colors`
--
ALTER TABLE `simple_colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
